After cloning the repo, if you are working in submodules, make sure to run the below command as a one-time setup, to automatically update your parent repository also to the commit of your submodules.
`git submodule foreach --recursive git config core.hooksPath .githooks/`

